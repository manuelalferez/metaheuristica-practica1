Resolución de un problema mediante el enfoque de tres algoritmos y comprobación 
de la eficiencia de los mismos: 

    - Algoritmo Greedy 
    - Algoritmo de búsqueda local del primer mejor 
    - Enfriamiento simulado
    
Las especificaciones solucitadas para el proyecto se encuentra en el siguiente 
enlace (PDF):
https://mega.nz/#!cGYzSYxS!hlPQ7_ddvh3tBgEB--rlyXET6XyRcLuqnTfI8q53k1A

El informe con la explicación del proyecto, así como la evaluación de los datos
obtenidos durante las ejecuciones se adjuntan en el siguiente enlace (PDF):
https://mega.nz/#!9CI3GSJJ!aK-sUMe_xcZtOPwzh8kHFhYPhwgvIH5HG2vCN7LX82k
