/**
 * @author Manuel Alférez Ruiz
 * @note Metaheurística Práctica 1
 */

package meta;

// Dependencies
import java.util.Random;

/**
 * @brief Clase que maneja la resolución del problema mediante un enfoque de Búsqueda Local del Primer Mejor
 */
public class BusquedaLocal {

    // mascara_DLB: contiene 0 o 1 y marca las zonas del entorno que se pueden visitar (0 si, 1 no)
    // no_Mejora: representa un estado en el que no hay ningún vecino mejor que la situación actual
    // coste_vecino y coste_Sact: costes del vecino y de la situación actual
    // vecino: vecino generado generado

    private int[] mascara_DLB;
    private boolean no_Mejora;
    private int iteraciones;
    private int coste_vecino;
    private int coste_Sact;
    private int[] vecino;
    private Fabrica fabrica;
    Random random;

    /**
     * @param permutacion Permutación a la que calcular el peso
     * @return El peso de la permutación
     * @brief Calcula el peso de una permutación
     */
    private int coste(int[] permutacion) {
        int peso = 0;
        for (int i = 0; i < fabrica.num_unidades; i++)
            for (int j = 0; j < fabrica.num_unidades; j++)
                if (i != j)
                    peso += fabrica.piezas[i][j] * fabrica.distancias[permutacion[i]][permutacion[j]];

        return peso;
    } // coste() -> Complejidad en tiempo= n²

    /**
     * @param tama El tamaño que tendrá la permutación inicial
     * @return Devuelve la situación actual inicial ya creada
     * @brief Genera la situación actual de inicio
     */
    private int[] genera_Sact(int tama) {

        int[] posiciones = new int[tama];
        for (int i = 0; i < tama; i++)
            posiciones[i] = i;

        int tama_logico = tama;
        int[] Sact = new int[tama];
        int posAleatoria;
        do {
            posAleatoria = random.nextInt(tama_logico);
            Sact[tama - tama_logico] = posiciones[posAleatoria];

            posiciones[posAleatoria] = posiciones[tama_logico - 1];
            tama_logico--;

        } while (tama_logico > 0);

        return Sact;
    } // genera_Sact() -> Complejidad en tiempo= n²

    /**
     * @param Sact Situación actual
     * @param r    Posición a intercambiar
     * @param s    Posición a intercambiar
     * @return Devuelve una permutación que contiene el intercambio
     * @brief Operador de intercambio
     */
    private int[] operadorInt(int[] Sact, int r, int s) {
        int c = Sact[r];
        Sact[r] = Sact[s];
        Sact[s] = c;

        return Sact;
    } // operadorInt() -> Complejidad en tiempo= 1

    /**
     * @param a    El vector origen
     * @param b    El vector destino
     * @param tama El tamaño de los vectores
     * @brief Realiza una copia de un vector
     */
    private void copia(int[] a, int[] b, int tama) {
        for (int i = 0; i < tama; i++)
            b[i] = a[i];
    } // copia() -> Complejidad en tiempo= n

    /**
     * @param Sact Situación actual
     * @return Devuelve el primer vecino mejor
     * @brief Realiza la búsqueda del primer vecino mejor:
     * Se realiza una permutación sobre la Sact y se elige la primera que se encuentre que sea mejor atendiendo al
     * siguiente proceder:
     * - Si la máscara se encuentra a 1 en dicha posición no se tiene en cuenta una permutación con dicho valor
     * ya que en una iteración anterior se hicieron todas las iteraciones y no hubo ningún vecino prometedor (mejor
     * que la Sact).
     * - Las permutaciones con la misma posición se obvian (i=j)
     * - A continuación se realiza la permutación correspondiente a la posición que haya pasado todos los anteriores
     * filtros
     * - Si el coste del vecino generado es mejor entonces se para la ejecución de la función y se devuelve dicha permutación
     */
    private int[] genera_vecino(int[] Sact) {
        // Marcará cuando el bucle principal de abajo tenga que detenerse
        boolean flag_parada = false;

        for (int i = 0; i < Sact.length; i++) {
            if (mascara_DLB[i] != 1) {
                for (int j = 0; j < Sact.length; j++) {
                    if (mascara_DLB[j] != 1) {
                        if (i != j) {
                            operadorInt(vecino, i, j);
                            coste_vecino = coste(vecino);
                            if ((coste_vecino - coste_Sact) < 0) {
                                iteraciones++;
                                if (iteraciones == 50000) {
                                    flag_parada = true;
                                    break;
                                }
                                flag_parada = true;
                                mascara_DLB[i] = 0;
                                mascara_DLB[j] = 0;
                                break;
                            } else {
                                operadorInt(vecino, i, j);
                            }
                        }
                    } // if mascara j

                } // for j

            } // if mascara i

            // Se ha generado el espacio al completo
            if (i == Sact.length - 1) {
                no_Mejora = true;
                mascara_DLB[i] = 1;
            }

            if (flag_parada)
                break;

        } // for i

        return vecino;
    } // genera_vecino()

    /**
     * @param fabrica Fabrica en la que aplicar el algoritmo
     * @return Devuelve la permutación solución del problema
     * @brief Algoritmo principal de búsqueda del primer mejor
     */
    public int[] busquedaLocalPrimerMejor(Fabrica fabrica) {

        random = new Random(Main.semilla);

        this.fabrica = fabrica;

        // Creación de la máscara
        mascara_DLB = new int[fabrica.num_unidades];

        // Inicialización de la máscara
        for (int i = 0; i < fabrica.num_unidades; i++)
            mascara_DLB[i] = 0;

        // Contador del número de iteraciones
        iteraciones = 0;

        no_Mejora = false;

        // Bucle del algoritmo
        int[] Sact = genera_Sact(fabrica.num_unidades);
        this.vecino = new int[Sact.length];
        copia(Sact, vecino, Sact.length);

        // Calculamos el coste de la Sact
        coste_Sact = coste(Sact);

        do {
            vecino = genera_vecino(Sact);
            Sact = vecino;
            coste_Sact = coste_vecino;
        } while (iteraciones < 50000 && !no_Mejora);

        return Sact;
    } // busquedaLocalPrimerMejor()

} // class BusquedaLocal
