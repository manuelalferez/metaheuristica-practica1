/**
 * @author Manuel Alférez Ruiz
 * @note Metaheurística Práctica 1
 */

package meta;

// Dependencies

import java.util.Random;

/**
 * @brief Clase que maneja la resolución del problema mediante un enfoque de Búsqueda Local de Enfriamiento Simulado
 */
public class EnfriamientoSimulado {

    // mascara_DLB: contiene 0 o 1 y marca las zonas del entorno que se pueden visitar (0 si, 1 no)
    // no_Mejora: representa un estado en el que no hay ningún vecino mejor que la situación actual
    // coste_vecino y coste_Sact: costes del vecino y de la situación actual
    // vecino: vecino generado generado

    private int[] mascara_DLB;
    private boolean no_Mejora;
    private Integer coste_vecino;
    private Integer coste_Sact;
    private int[] vecino;
    private Fabrica fabrica;
    private double T_actual, T_inicial;
    private int max_vecinos;
    int iteracion;
    int posActual;
    Random random;
    private static double cero = 0.0005;

    /**
     * @param permutacion Permutación a la que calcular el peso
     * @return El peso de la permutación
     * @brief Calcula el peso de una permutación
     */
    private int coste(int[] permutacion) {
        int peso = 0;
        for (int i = 0; i < fabrica.num_unidades; i++)
            for (int j = 0; j < fabrica.num_unidades; j++)
                if (i != j)
                    peso += fabrica.piezas[i][j] * fabrica.distancias[permutacion[i]][permutacion[j]];

        return peso;
    } // coste() -> Complejidad en tiempo= n²

    /**
     * @param tama El tamaño que tendrá la permutación inicial
     * @return Devuelve la situación actual inicial ya creada
     * @brief Genera la situación actual de inicio
     */
    private int[] genera_Sact(int tama) {
        int[] posiciones = new int[tama];
        for (int i = 0; i < tama; i++)
            posiciones[i] = i;

        int tama_logico = tama;
        int[] Sact = new int[tama];
        int posAleatoria;
        do {
            posAleatoria = random.nextInt(tama_logico);
            Sact[tama - tama_logico] = posiciones[posAleatoria];

            posiciones[posAleatoria] = posiciones[tama_logico - 1];
            tama_logico--;

        } while (tama_logico > 0);

        return Sact;
    } // genera_Sact() -> Complejidad en tiempo= n²

    /**
     * @param Sact Situación actual
     * @param r    Posición a intercambiar
     * @param s    Posición a intercambiar
     * @return Devuelve una permutación que contiene el intercambio
     * @brief Operador de intercambio
     */
    private int[] operadorInt(int[] Sact, int r, int s) {
        int c = Sact[r];
        Sact[r] = Sact[s];
        Sact[s] = c;

        return Sact;
    } // operadorInt() -> Complejidad en tiempo= 1

    /**
     * @param a    El vector origen
     * @param b    El vector destino
     * @param tama El tamaño de los vectores
     * @brief Realiza una copia de un vector
     */
    private void copia(int[] a, int[] b, int tama) {
        for (int i = 0; i < tama; i++)
            b[i] = a[i];
    } // copia() -> Complejidad en tiempo= n

    /**
     * @param Sact Situación actual
     * @return Devuelve el primer vecino mejor
     * @brief Realiza la búsqueda del primer vecino mejor:
     * Se realiza una permutación sobre la Sact y se elige la primera que se encuentre que sea mejor atendiendo al
     * siguiente proceder:
     * - Si la máscara se encuentra a 1 en dicha posición no se tiene en cuenta una permutación con dicho valor
     * ya que en una iteración anterior se hicieron todas las iteraciones y no hubo ningún vecino prometedor (mejor
     * que la Sact).
     * - Las permutaciones con la misma posición se obvian (i=j)
     * - A continuación se realiza la permutación correspondiente a la posición que haya pasado todos los anteriores
     * filtros
     * - Si el coste del vecino generado es mejor entonces se para la ejecución de la función y se devuelve dicha permutación
     */
    private int[] genera_vecino(int[] Sact) {
        // Marcará cuando el bucle principal de abajo tenga que detenerse
        boolean flag_parada = false;
        double Paceptacion;
        double umbralAceptacion;

        for (int i = posActual; i < Sact.length; i++) {
            if (mascara_DLB[i] != 1) {
                for (int j = posActual; j < Sact.length; j++) {
                    if (mascara_DLB[j] != 1) {
                        if (i != j) {
                            operadorInt(vecino, i, j);
                            coste_vecino = coste(vecino);
                            Paceptacion = 1 / Math.exp((coste_vecino - coste_Sact) / T_actual);
                            umbralAceptacion = random.nextDouble();

                            if (Paceptacion > umbralAceptacion) {
                                flag_parada = true;
                                if (mascara_DLB[i] == 1) {
                                    mascara_DLB[i] = 0;
                                    max_vecinos++;
                                }
                                if (mascara_DLB[j] == 1) {
                                    mascara_DLB[j] = 0;
                                    max_vecinos++;
                                }
                                posActual = j;
                                break;
                            } else {
                                operadorInt(vecino, i, j);
                            }
                        }
                    } // if mascara j

                } // for j

            } // if mascara i

            // Se ha generado el espacio al completo
            if (i == Sact.length - 1) {
                max_vecinos--;
                mascara_DLB[i] = 1;
                posActual = 0;
            }

            if (flag_parada)
                break;

        } // for i

        return vecino;
    } // genera_vecino()

    /**
     * @param temp Temperatura a la que aplicar el enfriamiento
     * @return La temperatura con el enfriamiento geométrico aplicado
     * @brief Mecanismo de enfriamiento geométrico
     */
    double g(double temp) {
        return temp * 0.9;
    }

    /**
     * @param k Número de iteración
     * @return La temperatura con el enfriamiento de Boltzmann aplicado
     * @brief Mecanismo de enfriamiento de Boltzmann
     */
    double b(int k) {
        return (T_inicial / (1 + (Math.log(k) / Math.log(2))));
    }

    /**
     * @param fabrica Fabrica en la que aplicar el algoritmo
     * @return Devuelve la permutación solución del problema
     * @brief Algoritmo principal del algoritmo de Enfriamiento Simulado
     */
    public int[] enfriamiento_Simulado(Fabrica fabrica) {
        random = new Random(Main.semilla);
        this.fabrica = fabrica;
        no_Mejora = false;

        // Creación de la máscara
        mascara_DLB = new int[fabrica.num_unidades];

        // Inicialización de la máscara
        for (int i = 0; i < fabrica.num_unidades; i++)
            mascara_DLB[i] = 0;

        // Bucle del algoritmo
        int[] Sact = genera_Sact(fabrica.num_unidades);
        this.vecino = new int[Sact.length];
        copia(Sact, vecino, Sact.length);

        // Calculamos el coste de la Sact
        coste_Sact = coste(Sact);

        T_actual = 1.5 * coste_Sact;
        T_inicial = T_actual;

        max_vecinos = fabrica.num_unidades;
        iteracion = 0;
        posActual = 0;

        do {
            vecino = genera_vecino(Sact);
            Sact = vecino;
            coste_Sact = coste_vecino;
            iteracion++;
            Main.informe_Enfriamiento_Simulado = Main.informe_Enfriamiento_Simulado + iteracion + ".  " + coste_Sact.toString() + "\n";

            if (Main.tipo == 1) {
                T_actual = g(T_actual);
            } else {
                T_actual = b(iteracion);
            }
        } while ((T_actual > cero && (0.1 * max_vecinos) > cero));

        return Sact;

    } // enfriamiento_Simulado()

} // class EnfriamientoSimulado
