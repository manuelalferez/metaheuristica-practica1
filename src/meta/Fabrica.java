/**
 * @author Manuel Alférez Ruiz
 * @note Metaheurística Práctica 1
 */

package meta;

// Dependencies
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

/**
 * @brief Clase que almacena la información de un fábrica
 */
public class Fabrica {

    // Matriz piezas: Representa el flujo de piezas entre cada par de unidades de producción
    // Matriz distancias: Representa las distancias entre cada par de unidades de producción
    // num_unidades: Representa el número de unidades de producción (la dimensión de las matrices)
    protected int[][] piezas;
    protected int[][] distancias;
    protected int num_unidades;

    /**
     * @brief Constructor
     * @param _direccion Dirección donde se encuentra el archivo con los datos
     * @post Crea una fábrica
     *
     */
    public Fabrica(String _direccion) {

        File archivo;
        FileReader fr = null;
        BufferedReader br;

        try {
            // Apertura del fichero y creación de BufferedReader para poder
            // hacer una lectura cómoda (disponer del método readLine()).
            archivo = new File(_direccion);
            fr = new FileReader(archivo);
            br = new BufferedReader(fr);

            String linea;

            // Lectura de la dimensión de la matriz
            // .replaceAll: Reemplaza varios espacios por uno y elimina los del comienzo y el final
            linea = br.readLine().replaceAll(" +", " ").trim();;
            num_unidades = Integer.parseInt(linea);

            br.readLine(); //Eliminamos la línea en blanco

            // Creación de las matrices
            piezas= new int[num_unidades][num_unidades];
            distancias= new int[num_unidades][num_unidades];

            // Inicialización de las matrices
            for(int i=0;i<num_unidades;i++)
                for(int j=0;j<num_unidades;j++) {
                    piezas[i][j] = 0;
                    distancias[i][j] = 0;
                }

            // Lectura de la matriz de piezas

            int fil = 0; // Para fijar la fila

            while ((linea = br.readLine()) != null && fil<num_unidades) {
                // Para leer cada dígito de una línea, dividimos la línea en "partes"
                // (cada número de la fila será considerado una "parte")
                String linea_formateada = linea.replaceAll(" +", " ").trim();
                String[] parts = linea_formateada.split(" ");
                for (int j = 0; j < num_unidades; j++){
                    piezas[fil][j] = Integer.parseInt(parts[j]);
                }
                fil++;
            }

            // Lectura de la matriz de distancias

            fil = 0;

            while ((linea = br.readLine()) != null && fil<num_unidades) {
                String linea_formateada = linea.replaceAll(" +", " ").trim();
                String[] parts = linea_formateada.split(" ");
                for (int j = 0; j < num_unidades; j++){
                    distancias[fil][j] = Integer.parseInt(parts[j]);
                }
                fil++;
            }


        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // En el finally cerramos el fichero, para asegurarnos
            // que se cierra tanto si cuando va bien como si salta
            // una excepción.
            try {
                if (null != fr) {
                    fr.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }

    } // constructor Fabrica()

} // class Fabrica
