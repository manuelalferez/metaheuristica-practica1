/**
 * @author Manuel Alférez Ruiz
 * @note Metaheurística Práctica 1
 */

package meta;

// Dependencies

import java.util.Scanner;
import java.io.*;


/**
 * @brief Función principal
 */
public class Main {

    // Semillas
    public static int semilla0 = 53911043;
    public static int semilla1 = 12345678;
    public static int semilla2 = 34567812;
    public static int semilla3 = 45678123;
    public static int semilla4 = 56781234;
    public static int[] semillas = new int[5];

    // Semilla escogida
    public static int semilla;

    // Enfriamiento Simulado:
    //      1=Geométrico
    //      2=Boltzmann
    public static int tipo;

    // Almacena los nombres de los dos tipos de enfriamientos
    public static String[] nombres_Tipo = new String[2];
    // Almacena los nombres de los 10 archivos de datos
    public static String[] nombreArchivo = new String[10];
    public static String informe_Enfriamiento_Simulado;

    /**
     * @param mensaje Mensaje a escribir en el fichero
     * @brief Método que escribe en un fichero
     * @post Escribir un mensaje en un fichero llamado solucion.txt
     */
    public static void escribirFichero(String nombre_Archivo, String mensaje) {
        FileWriter fichero = null;
        PrintWriter pw;
        try {
            fichero = new FileWriter(nombre_Archivo);
            pw = new PrintWriter(fichero);
            pw.println(mensaje);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                // Para asegurarnos que se cierra el fichero
                if (null != fichero)
                    fichero.close();
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    } // escribirFichero()

    /**
     * @param solucion Vector con la solución del problema
     * @param fabrica  Fabrica con las matrices de piezas transferidas y las distancias entre unidades de producción
     * @return El peso de la solucion
     * @brief Calcula el peso de la solución
     */
    public static int calcularPeso(int[] solucion, Fabrica fabrica) {
        int peso = 0;
        for (int i = 0; i < solucion.length; i++)
            for (int j = 0; j < solucion.length; j++)
                if (i != j)
                    peso += fabrica.piezas[i][j] * fabrica.distancias[solucion[i]][solucion[j]];

        return peso;
    } // calcularPeso()

    public static void main(String[] args) {

        // Asignación de archivos y semillas a los vectores
        nombreArchivo[0] = "cnf01.dat";
        nombreArchivo[1] = "cnf02.dat";
        nombreArchivo[2] = "cnf03.dat";
        nombreArchivo[3] = "cnf04.dat";
        nombreArchivo[4] = "cnf05.dat";
        nombreArchivo[5] = "cnf06.dat";
        nombreArchivo[6] = "cnf07.dat";
        nombreArchivo[7] = "cnf08.dat";
        nombreArchivo[8] = "cnf09.dat";
        nombreArchivo[9] = "cnf10.dat";

        semillas[0] = semilla0;
        semillas[1] = semilla1;
        semillas[2] = semilla2;
        semillas[3] = semilla3;
        semillas[4] = semilla4;

        // Declaración de variables
        String direccion;
        Fabrica fabrica;
        nombres_Tipo[0] = "Geométrico";
        nombres_Tipo[1] = "Boltzmann";
        Suma suma;
        Greedy greedy;
        BusquedaLocal primerMejor;
        EnfriamientoSimulado enfriamientoS;

        int[] solucion_Greedy, solucion_BL, solucion_ES_g, solucion_ES_b;
        long t_Greedy, t_BL, t_ES_g, t_ES_b;
        int iteracion_ES_g, iteracion_ES_b;

        int pesoSolucionGreedy, pesoSolucion_BL, pesoSolucion_ES_g, pesoSolucion_ES_b;
        int opcion, archivo; // Opción y archivo elegido en el menu
        Scanner reader = new Scanner(System.in);


        long T_INICIO, T_FIN; //Variables para determinar el tiempo de ejecución

        do {
            System.out.printf("\n");
            System.out.printf("--------------------MENU--------------------\n");
            System.out.printf("1. Calcular solución: 1 archivo con semilla única.\n");
            System.out.printf("2. Calcular solución: 1 archivo con todas las semillas.\n");
            System.out.printf("3. Salir\n");
            System.out.printf("--------------------------------------------\n\n");

            opcion = reader.nextInt(); // Lectura de la opción elegida

            switch (opcion) {
                case 1:
                    System.out.printf("---------ELEGIR ARCHIVO:--------\n");
                    System.out.printf("cnf01.dat = 1, cnf02.dat = 2 ...\n");
                    System.out.printf("--------------------------------\n\n");
                    archivo = reader.nextInt();

                    System.out.printf("---------ELEGIR SEMILLA:--------\n");
                    System.out.printf("0. 53911043;\n");
                    System.out.printf("1. 12345678;\n");
                    System.out.printf("2. 34567812;\n");
                    System.out.printf("3. 45678123;\n");
                    System.out.printf("4. 56781234;\n");
                    System.out.printf("---------------------------------\n\n");
                    semilla = reader.nextInt();
                    semilla = semillas[semilla]; // Asignación de la semilla

                    // Creación de la fábrica y de la dirección del archivo elegido
                    direccion = "datos/" + nombreArchivo[archivo - 1];
                    fabrica = new Fabrica(direccion);

                    System.out.printf("\nArchivo: " + nombreArchivo[archivo - 1] + " con Semilla: " + semilla + "\n");

                    /*------------------CÁLCULOS------------------*/

                    // Cálculo de la suma de Piezas y suma de Distancias
                    suma = new Suma(fabrica);

                    // Cálculo de la solución Greedy
                    greedy = new Greedy();
                    T_INICIO = System.currentTimeMillis();
                    solucion_Greedy = greedy.algoritmoGreedy(suma);
                    T_FIN = System.currentTimeMillis();
                    t_Greedy = T_FIN - T_INICIO;

                    // Cálculo de la solución de Búsqueda local Primer el mejor
                    primerMejor = new BusquedaLocal();
                    T_INICIO = System.currentTimeMillis();
                    solucion_BL = primerMejor.busquedaLocalPrimerMejor(fabrica);
                    T_FIN = System.currentTimeMillis();
                    t_BL = T_FIN - T_INICIO;

                    // Cálculo de la solución de la Búsqueda Local con Enfriamiento Simulado (Geométrico)
                    tipo = 1;
                    informe_Enfriamiento_Simulado = "Trayectoria de costes en cada iteración (" + nombres_Tipo[Main.tipo - 1] + "): \n";
                    enfriamientoS = new EnfriamientoSimulado();
                    T_INICIO = System.currentTimeMillis();
                    solucion_ES_g = enfriamientoS.enfriamiento_Simulado(fabrica);
                    T_FIN = System.currentTimeMillis();
                    t_ES_g = T_FIN - T_INICIO;
                    iteracion_ES_g = enfriamientoS.iteracion;

                    // Cálculo de la solución de la Búsqueda Local con Enfriamiento Simulado (Boltzmann)
                    tipo = 2;
                    informe_Enfriamiento_Simulado = informe_Enfriamiento_Simulado + "Trayectoria de costes en cada iteración" +
                            " (" + nombres_Tipo[Main.tipo - 1] + "): \n";
                    enfriamientoS = new EnfriamientoSimulado();
                    T_INICIO = System.currentTimeMillis();
                    solucion_ES_b = enfriamientoS.enfriamiento_Simulado(fabrica);
                    T_FIN = System.currentTimeMillis();
                    t_ES_b = T_FIN - T_INICIO;
                    iteracion_ES_b = enfriamientoS.iteracion;

                    // Escribimos en fichero los distintos costes por los que pasó el algoritmo
                    Main.escribirFichero("enfriamiento_simulado.log", informe_Enfriamiento_Simulado);

                    // Cálculo de pesos
                    pesoSolucionGreedy = calcularPeso(solucion_Greedy, fabrica);
                    pesoSolucion_BL = calcularPeso(solucion_BL, fabrica);
                    pesoSolucion_ES_g = calcularPeso(solucion_ES_g, fabrica);
                    pesoSolucion_ES_b = calcularPeso(solucion_ES_b, fabrica);

                    /*------------------IMPRESIONES------------------*/

                    // Impresión del vector de la suma de Piezas
                    System.out.printf("Suma piezas: \n");
                    for (int i = 0; i < fabrica.num_unidades; i++) {
                        System.out.printf("%d ", suma.sumaPiezas[i]);
                    }

                    // Impresión del vector de la suma de Distancias
                    System.out.printf("\n\nSuma distancias: \n");
                    for (int i = 0; i < fabrica.num_unidades; i++) {
                        System.out.printf("%d ", suma.sumaDistancias[i]);
                    }

                    // Impresión de la solución Greedy
                    System.out.printf("\n\nSolución Greedy: \n");
                    for (int i = 0; i < fabrica.num_unidades; i++) {
                        System.out.printf("%d ", solucion_Greedy[i]);
                    }

                    // Impresión de la solución de Búsqueda local Primer el mejor
                    System.out.printf("\n\nSolución Búsqueda local primer mejor: \n");
                    for (int i = 0; i < fabrica.num_unidades; i++) {
                        System.out.printf("%d ", solucion_BL[i]);
                    }

                    // Impresión de la solución de la Búsqueda Local con Enfriamiento Simulado
                    System.out.printf("\n\nSolución Enfriamiento Simulado (Geométrico): \n");
                    for (int i = 0; i < fabrica.num_unidades; i++) {
                        System.out.printf("%d ", solucion_ES_g[i]);
                    }

                    // Impresión de la solución de la Búsqueda Local con Enfriamiento Boltzmann
                    System.out.printf("\n\nSolución Enfriamiento Simulado (Boltzmann): \n");
                    for (int i = 0; i < fabrica.num_unidades; i++) {
                        System.out.printf("%d ", solucion_ES_b[i]);
                    }

                    // Impresión de pesos y tiempos
                    System.out.printf("\n\n**********************************************\n");

                    System.out.printf("Coste Greedy:                %d    Tiempo: %d  ms\n", pesoSolucionGreedy, t_Greedy);
                    System.out.printf("Coste Primer Mejor:          %d     Tiempo: %d ms\n", pesoSolucion_BL, t_BL);
                    System.out.printf("Coste Enfriamiento Simulado  (%s): %d     Tiempo: %d  ms\n",
                            nombres_Tipo[0], pesoSolucion_ES_g, t_ES_g);
                    System.out.printf("     -Iteración: %d\n", iteracion_ES_g);
                    System.out.printf("Coste Enfriamiento Simulado  (%s): %d     Tiempo: %d  ms\n",
                            nombres_Tipo[1], pesoSolucion_ES_b, t_ES_b);
                    System.out.printf("     -Iteración: %d\n", iteracion_ES_b);

                    System.out.printf("**********************************************\n");

                    break;

                case 2:
                    System.out.printf("---------ELEGIR ARCHIVO:--------\n");
                    System.out.printf("cnf01.dat = 1, cnf02.dat = 2 ...\n");
                    System.out.printf("---------------------------------\n\n");
                    archivo = reader.nextInt();

                    String mensaje = ""; // texto con las solución que será introducido en el archivo solucion.txt

                    // Creación de la fábrica y de la dirección del archivo elegido
                    direccion = "datos/" + nombreArchivo[archivo - 1];
                    fabrica = new Fabrica(direccion);

                    /*------------------CÁLCULOS------------------*/

                    for (int k = 0; k < semillas.length; k++) {
                        semilla = semillas[k];
                        // Cálculo de la suma de Piezas y suma de Distancias
                        suma = new Suma(fabrica);

                        // Cálculo de la solución Greedy
                        greedy = new Greedy();
                        T_INICIO = System.currentTimeMillis();
                        solucion_Greedy = greedy.algoritmoGreedy(suma);
                        T_FIN = System.currentTimeMillis();
                        t_Greedy = T_FIN - T_INICIO;

                        // Cálculo de la solución de Búsqueda local Primer el mejor
                        primerMejor = new BusquedaLocal();
                        T_INICIO = System.currentTimeMillis();
                        solucion_BL = primerMejor.busquedaLocalPrimerMejor(fabrica);
                        T_FIN = System.currentTimeMillis();
                        t_BL = T_FIN - T_INICIO;

                        // Cálculo de la solución de la Búsqueda Local con Enfriamiento Simulado (Geométrico)
                        tipo = 1;
                        enfriamientoS = new EnfriamientoSimulado();
                        T_INICIO = System.currentTimeMillis();
                        solucion_ES_g = enfriamientoS.enfriamiento_Simulado(fabrica);
                        T_FIN = System.currentTimeMillis();
                        t_ES_g = T_FIN - T_INICIO;
                        iteracion_ES_g = enfriamientoS.iteracion;

                        // Cálculo de la solución de la Búsqueda Local con Enfriamiento Simulado (Boltzmann)
                        tipo = 2;
                        enfriamientoS = new EnfriamientoSimulado();
                        T_INICIO = System.currentTimeMillis();
                        solucion_ES_b = enfriamientoS.enfriamiento_Simulado(fabrica);
                        T_FIN = System.currentTimeMillis();
                        t_ES_b = T_FIN - T_INICIO;
                        iteracion_ES_b = enfriamientoS.iteracion;

                        // Cálculo de pesos
                        pesoSolucionGreedy = calcularPeso(solucion_Greedy, fabrica);
                        pesoSolucion_BL = calcularPeso(solucion_BL, fabrica);
                        pesoSolucion_ES_g = calcularPeso(solucion_ES_g, fabrica);
                        pesoSolucion_ES_b = calcularPeso(solucion_ES_b, fabrica);

                        /*------------------IMPRESIONES------------------*/
                        mensaje = mensaje + "\nArchivo: " + nombreArchivo[archivo - 1] + " con Semilla: " + semilla + "\n";
                        mensaje = mensaje + "Suma piezas: \n";
                        for (int i = 0; i < fabrica.num_unidades; i++) {
                            mensaje = mensaje + suma.sumaPiezas[i] + " ";
                        }

                        mensaje = mensaje + "\n\nSuma distancias: \n";
                        for (int i = 0; i < fabrica.num_unidades; i++) {
                            mensaje = mensaje + suma.sumaDistancias[i] + " ";
                        }

                        mensaje = mensaje + "\n\nSolución Greedy: \n";
                        for (int i = 0; i < fabrica.num_unidades; i++) {
                            mensaje = mensaje + solucion_Greedy[i] + " ";
                        }

                        mensaje = mensaje + "\n\nSolución Búsqueda local primer mejor: \n";
                        for (int i = 0; i < fabrica.num_unidades; i++) {
                            mensaje = mensaje + solucion_BL[i] + " ";
                        }

                        // Impresión de la solución de la Búsqueda Local con Enfriamiento Simulado
                        mensaje = mensaje + "\n\nSolución Enfriamiento Simulado (Geométrico): \n";
                        for (int i = 0; i < fabrica.num_unidades; i++) {
                            mensaje = mensaje + solucion_ES_g[i] + " ";
                        }

                        // Impresión de la solución de la Búsqueda Local con Enfriamiento Boltzmann
                        mensaje = mensaje + "\n\nSolución Enfriamiento Simulado (Boltzmann): \n";
                        for (int i = 0; i < fabrica.num_unidades; i++) {
                            mensaje = mensaje + solucion_ES_b[i] + " ";
                        }

                        mensaje = mensaje +
                                "\nCoste Greedy:                             " + pesoSolucionGreedy + "   Tiempo: " + t_Greedy + " ms\n" +
                                "Coste Primer Mejor:                       " + pesoSolucion_BL + "    Tiempo: " + t_BL + " ms\n" +
                                "Coste Enfriamiento Simulado (" + nombres_Tipo[0] + "):  " + pesoSolucion_ES_g + "    Tiempo: " + t_ES_g + " ms\n" +
                                "     -Iteración: " + iteracion_ES_g + "\n" +
                                "Coste Enfriamiento Simulado (" + nombres_Tipo[1] + "):  " + pesoSolucion_ES_b + "    Tiempo: " + t_ES_b + " ms\n" +
                                "     -Iteración: " + iteracion_ES_b + "\n";

                        mensaje = mensaje + "\n**********************************************\n\n";

                    } // for k
                    escribirFichero("solucion.txt", mensaje);
                    System.out.printf("Hecho. En el archivo soluciones.txt tiene sus soluciones.\n");

                    break;
            } // switch

        } while (opcion != 3);

    } // main()

} // class Main
